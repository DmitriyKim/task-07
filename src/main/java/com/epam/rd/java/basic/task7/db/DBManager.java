package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	public static final String SELECT_ALL_USERS = "SELECT id,login FROM users ORDER BY id";
	public static final String SELECT_USER_BY_LOGIN = "SELECT id,login FROM users WHERE login=? ORDER BY id";
	public static final String ADD_USER = "insert into users (login) values (?)";
	public static final String DELETE_USERS_BY_ID= "DELETE FROM users WHERE id=?";

	public static final String SELECT_ALL_TEAMS = "SELECT id,name FROM teams ORDER BY id";
	public static final String SELECT_TEAM_BY_NAME= "SELECT id,name FROM teams WHERE name=? ORDER BY id";
	public static final String UPDATE_TEAM_BY_ID= "UPDATE teams SET name=? where id=?";
	public static final String DELETE_TEAM_BY_ID= "DELETE FROM teams WHERE id=?";
	public static final String ADD_TEAM = "insert into teams (name) values (?)";
	public static final String ADD_TEAM_USERS = "insert into users_teams (user_id,team_id) values (?,?)";
	public static final String SELECT_USERS_TEAMS_BY_USER =
			"SELECT t.id,t.name " +
					"FROM users_teams ut JOIN teams t ON ut.team_id=t.id " +
					"AND ut.user_id =? " +
					"ORDER BY id";
	private static DBManager instance;
	private final String connectionURL;

	public static synchronized DBManager getInstance() {
		if (instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		Properties properties = new Properties();
		try(FileReader in = new FileReader("app.properties")){
			properties.load(in);
			connectionURL = properties.getProperty("connection.url");
		} catch (IOException e) {
		e.printStackTrace();
		throw new IllegalStateException(e);
		}
	}

	private Connection getConnection() throws SQLException {
		return getConnection(true);
	}

	private Connection getConnection(boolean autocommit) throws SQLException {
		Connection con = DriverManager.getConnection(connectionURL);
		con.setAutoCommit(autocommit);
		con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		return con;
	}

	private void rollback(Connection con, Exception e) throws DBException {
		try {
			if (con != null) {
				con.rollback();
			}
		} catch (SQLException ex) {
			ex.addSuppressed(e);
			ex.printStackTrace();
			throw new DBException("addReceipt, can not rollback connection", ex);
		}
	}

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<User> findAllUsers() throws DBException {
		try(Connection con = getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(SELECT_ALL_USERS);) {
			List<User> users = new ArrayList<>();
			while (rs.next()){
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setLogin(rs.getString("login"));
				users.add(u);
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(ADD_USER,Statement.RETURN_GENERATED_KEYS);)
		{
			st.setString(1,user.getLogin());
			st.executeUpdate();
			ResultSet keys = st.getGeneratedKeys();
			if (keys.next()) {
				user.setId(keys.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("can not add user", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(DELETE_USERS_BY_ID);) {
			for (User u : users) {
				st.setLong(1, u.getId());
				st.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public User getUser(String login) throws DBException {
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(SELECT_USER_BY_LOGIN);) {
			st.setString(1,login);
			try(ResultSet rs = st.executeQuery()) {
				User user = new User();
				if (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setLogin(rs.getString("login"));
					return user;
				}
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(SELECT_TEAM_BY_NAME);) {
			st.setString(1,name);
			try(ResultSet rs = st.executeQuery()) {
				Team team = new Team();
				if (rs.next()) {
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
					return team;
				}
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try(Connection con = getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(SELECT_ALL_TEAMS);) {
			List<Team> teams = new ArrayList<>();
			while (rs.next()){
				Team t = new Team();
				t.setId(rs.getInt("id"));
				t.setName(rs.getString("name"));
				teams.add(t);
			}
			return teams;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(ADD_TEAM,Statement.RETURN_GENERATED_KEYS);)
		{
			st.setString(1,team.getName());
			st.executeUpdate();
			ResultSet keys = st.getGeneratedKeys();
			if (keys.next()) {
				team.setId(keys.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("can not add user", e);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try {
			con = getConnection(false);
			PreparedStatement st = con.prepareStatement(ADD_TEAM_USERS);
			for (Team t : teams) {
				int k=0;
				st.setLong(++k, user.getId());
				st.setLong(++k, t.getId());
				st.executeUpdate();
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con, e);
			throw new DBException("can not add user", e);
		} finally {
			close(con);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(SELECT_USERS_TEAMS_BY_USER);) {
			st.setLong(1,user.getId());
			List<Team> teams = new ArrayList<>();
			try(ResultSet rs = st.executeQuery()) {
				while (rs.next()) {
					Team t = new Team();
					t.setId(rs.getInt("id"));
					t.setName(rs.getString("name"));
					teams.add(t);
				}
			}
			return teams;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
//		Team t =getTeam(team.getName());
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(DELETE_TEAM_BY_ID);) {
			st.setLong(1,team.getId());
			st.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops, I did it again ",e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
//		Team t =getTeam(team.getName());
		try(Connection con = getConnection();
			PreparedStatement st = con.prepareStatement(UPDATE_TEAM_BY_ID);)
		{
			st.setString(1,team.getName());
			st.setLong(2,team.getId());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("can not add team", e);
		}
		return true;
	}

}
